const zmq = require("zeromq");
const socket = zmq.socket("sub"); // SUB socket

socket.connect(`tcp://127.0.0.1:3000`);

socket.subscribe(`publisher`);

socket.on(`message`, function (pub, tempo, count) {
	console.log(`Recebendo de ${pub}: Tempo: ${tempo} - Contador: ${count}`);
});