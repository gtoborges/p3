var zmq = require("zeromq");
var socket = zmq.socket("pub"); // aqui definimos um socket do tipo PUBlisher

socket.bindSync(`tcp://127.0.0.1:3000`); // endereço do publisher definido

const topic = `publisher` // Criando um tópico que será publicado
let contador = 0;

setInterval(function () {
	const timestamp = new Date().toLocaleTimeString()
    socket.send([topic, timestamp, contador++]); // conteúdo enviado pelo publisher
}, 1000);