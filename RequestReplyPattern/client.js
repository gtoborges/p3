const zmq = require("zeromq")
const socket = zmq.socket("req") //aqui agora definimos um socket do tipo REQuest

socket.connect(`tcp://127.0.0.1:3000`) // mesmo endereço definido no server

let contador = 0

socket.on(`message`, (msg) => {
    console.log(`Resposta recebida: \"${msg}\"`)
    setTimeout(enviarMensagem, 1000)
})

enviarMensagem()

function enviarMensagem() {
    let msg = `Mensagem: #${contador++}`
    console.log(`Enviando: ${msg}`)
    socket.send(msg)
}