const zmq = require("zeromq")
const socket = zmq.socket("rep") //aqui agora definimos um socket do tipo REsPonse

socket.bindSync(`tcp://127.0.0.1:3000`) //determina o endereço de conexão

socket.on(`message`, (msg) => { // evento disparado quando uma mensagem é recebida
    console.log(`Mensagem recebida: \"${msg}\"`) // imprimindo mensagem recebida
    socket.send(`Respondendo \"${msg}\" !!!`) // enviando uma resposta
})