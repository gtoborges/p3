const zmq = require("zeromq")
const socket = zmq.socket("pull")

socket.connect(`tcp://127.0.0.1:3000`) // definimos aqui o endereço de conexão, que deve ser o mesmo do server

socket.on(`message`, (msg) => {
    console.log(`Mensagem recebida: ${msg}`)
})