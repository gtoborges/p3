const zmq = require("zeromq") // aqui importamos a biblioteca do ZeroMQ
const socket = zmq.socket("push") // aqui criamos o socket, do tipo PUSH, que tem um comportamento específico

socket.bindSync(`tcp://127.0.0.1:3000`) // definimos aqui o endereço que a conexão será estabelecida

let contador = 0

setInterval( () => { // Função que se repete no espaço de tempo definido em milésimos
    const mensagem = `Contador: #${contador++}`; // criamos uma mensagem que será enviada pelo socket
    console.log(`Enviando mensagem: ${mensagem}`) // imprimimos a mensagem no console do server
    socket.send(mensagem) // enviamos a mensagem
}, 1000)