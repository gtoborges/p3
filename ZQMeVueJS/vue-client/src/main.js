import Vue from 'vue'
import App from './App.vue'
import VueSocketIO from 'vue-socket.io-extended'
import io from 'socket.io-client'

export const SocketInstance = io('http://localhost:4000')
Vue.use(VueSocketIO, SocketInstance)

import SocketZero from './SocketZero'
Vue.component('SocketZero', SocketZero)


new Vue({
  el: '#app',
  render: h => h(App)
})
